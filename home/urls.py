from django.urls import path

from home import views

urlpatterns = [
    path('page/', views.homepage, name='page'),
    path('all_students/', views.home, name='list_of_student'),
    path('all_brands/', views.brand, name='list_of_brands'),
    path('', views.HomeTemplateView.as_view(), name='homepage')
]
#link all_brands/, 2. motoda, 3.url intern, ma folosesc in HTML de el
