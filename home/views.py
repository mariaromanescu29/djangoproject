from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView


def homepage(request):
    return HttpResponse('Hello, Maria')


def home(request):
    context = {
        'all_students': [
            {
                'first_name': 'Maria',
                "last_name": 'Romanescu',
                'age': 24
            },
            {
              'first_name': 'Mircea',
               'last_name': 'Zainea',
               'age': 32
            }
        ]

    }
    return render(request, 'home/home.html', context)


def brand(request):
    context = {
        'cars': [
            {
                'Infiintare': 2003,
                "CA": 31_540_000_000,
                'Nume': 'Tesla'
            },
            {
                'Infiintare': 1937,
                "CA": 126_800_000_000,
                'Nume': 'Tesla'
            }
        ]

    }
    return render(request, 'home/brand.html', context)


class HomeTemplateView(TemplateView):
    template_name = 'home/homepage.html'