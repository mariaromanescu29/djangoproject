from django.apps import AppConfig


class ParissionConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'permission'
