from django.db import models

from course.models import Course
from teacher.models import Teacher


class Student(models.Model):
    gender_choices = (
        ('Male', 'Male'),
        ('Female','Female'),
        ('Other', 'Other')
    )

    first_name = models.CharField(max_length=30, null=True)
    last_name = models.CharField(max_length=30)
    age = models.IntegerField()
    date_of_birth = models.DateField()
    olympic = models.BooleanField(default=False)
    gender = models.CharField(max_length=10, choices=gender_choices)
    active = models.BooleanField(default=True)
    created_at = models.DateField(auto_now_add=True)
    update_at = models.DateField(auto_now=True)
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE, null=True)
    course = models.ForeignKey(Course, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return f'{self.first_name}{self.last_name}'
