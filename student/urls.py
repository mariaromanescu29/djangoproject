from django.urls import path, include
from rest_framework import routers

from student import views
from student.views import StudentViewSet

router = routers.DefaultRouter()
router.register('api-student', StudentViewSet)


urlpatterns = [
    path('create_student/', views.StudentCreateView.as_view(), name='create-student'),
    path('list_of_students/', views.StudentListView.as_view(), name='list-of-student'),
    path('update_student/<int:pk>', views.StudentUpdateView.as_view(), name='update-student'), #int = in pk=primary key; trebuie sa stim ID-ul (din baza de date) inregistrarii
    path('delete_student/<int:pk>', views.StudentDeleteView.as_view(), name='delete-student'),
    path('detail_student/<int:pk>', views.StudentDetailView.as_view(), name='detail-student'),
    path('', include(router.urls))
]

