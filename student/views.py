from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView
from rest_framework import viewsets

from student.forms import StudentForm
from student.models import Student
from student.serializers import StudentSerializer


class StudentCreateView(LoginRequiredMixin, PermissionRequiredMixin,CreateView):
    template_name = 'student/create_student.html' #calea catre fisierul HTML unde va fi afisat formularul
    model = Student    #modelul=tabelul pe care il folosim in generarea formularului
    success_url = reverse_lazy('create-student') #redirectionarea dupa ce formularul a fost salvat
    form_class = StudentForm
    permission_required = 'student.add_student'


class StudentListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'student/list_students.html'
    model = Student
    context_object_name = 'all_students'
    permission_required = 'student.view_list_of_students'

    def get_context_data(self, **kwargs):
        data = super(StudentListView, self).get_context_data(**kwargs)
        all_students = Student.objects.all()
        from student.filters import StudentFilters
        my_filter = StudentFilters(self.request.GET, queryset=all_students)
        all_students = my_filter.qs
        data['all_students'] = all_students
        data['my_filter'] = my_filter
        return data



    # def get_queryset(self):
    #     return Student.objects.all().order_by('first_name')

class StudentUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'student/update_student.html'
    model = Student #modelul de unde iau inregistrarea
    success_url = reverse_lazy('list-of-student') #von redirectionati catre lista de studenti
    form_class = StudentForm # ca sa avem customizarea
    permission_required = 'student.change_student'


class StudentDeleteView(LoginRequiredMixin, DeleteView):
    template_name = 'student/delete_student.html'
    model = Student
    success_url = reverse_lazy('list-of-student')


class StudentDetailView(LoginRequiredMixin, DetailView):
    template_name = 'student/detail_student.html'
    model = Student


class StudentViewSet(viewsets.ModelViewSet):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer