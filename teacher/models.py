from django.db import models


class Teacher(models.Model):
    first_name = models.CharField(max_length=30, null=True)
    last_name = models.CharField(max_length=30, null=True)
    specialisation = models.CharField(max_length=30)
    active = models.BooleanField(default=True)
    created_at = models.DateField(auto_now_add=True)
    update_at = models.DateField(auto_now=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name} - {self.specialisation}'


