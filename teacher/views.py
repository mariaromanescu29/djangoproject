from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, DeleteView, UpdateView, DetailView
from rest_framework import viewsets

from student.models import Student
from teacher.filters import TeacherFilters
from teacher.forms import TeacherForm
from teacher.models import Teacher
from teacher.serializers import TeacherSerializer


class TeacherCreateView(LoginRequiredMixin, CreateView):
    template_name = 'teacher/create_teacher.html'
    model = Teacher
    success_url = reverse_lazy('create-teacher')
    form_class = TeacherForm


class TeacherListView(LoginRequiredMixin, ListView):
    template_name = 'teacher/list_teacher.html'
    model = Teacher
    context_object_name = 'all_teachers'

    def get_context_data(self, **kwargs):
        data = super(TeacherListView, self).get_context_data(**kwargs)
        all_teachers = Teacher.objects.all()
        my_filter = TeacherFilters(self.request.GET, queryset=all_teachers)
        all_teachers = my_filter.qs
        data['all_teachers'] = all_teachers
        data['my_filter'] = my_filter
        return data


class TeacherUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'teacher/update_teacher.html'
    model = Teacher
    success_url = reverse_lazy('list-teacher')  # pagina de redirectionare
    form_class = TeacherForm
    permission_required = 'teacher.change_teacher'


class TeacherDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'teacher/delete_teacher.html'
    model = Teacher
    success_url = reverse_lazy('list-teacher')
    permission_required = 'teacher.delete_teacher'


class TeacherDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'teacher/detail_teacher.html'
    model = Teacher
    permission_required = 'teacher.detail_teacher'


def get_all_students_per_teacher(request, id_teacher):
    all_students_per_teacher = Student.objects.filter(teacher_id=id_teacher)
    context = {'students': all_students_per_teacher}
    return render(request, 'student/get_all_student_per_teacher.html', context)


class TeacherViewSet(viewsets.ModelViewSet):
    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer


